npm install -g serve

npm install -g gatsby-cli

Develop

- gatsby develop

Production

- gatsby build

- gatsby serve

Netlify

- netlify init
- netlify deploy --dir=public -p
