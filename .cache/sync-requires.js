const { hot } = require("react-hot-loader/root")

// prefer default export if available
const preferDefault = m => m && m.default || m


exports.components = {
  "component---src-templates-contador-js": hot(preferDefault(require("/mnt/HD2/dev/powersites/src/templates/contador.js"))),
  "component---src-pages-contato-js": hot(preferDefault(require("/mnt/HD2/dev/powersites/src/pages/contato.js"))),
  "component---src-pages-index-js": hot(preferDefault(require("/mnt/HD2/dev/powersites/src/pages/index.js")))
}

