import React from "react";

const Contador = context => {
  return (
    <div>
      <h1>Contador</h1>
      <p>{context.pageContext.page}</p>
    </div>
  );
};

export default Contador;
