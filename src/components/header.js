import React, { useState, useEffect } from "react";

const Header = () => {
  const [i, setI] = useState(0);

  useEffect(() => {
    setInterval(() => {
      setI(valorAntigo => valorAntigo + 1);
    }, 1000);
  }, []);

  return <h1>Header {i}</h1>;
};

export default Header;
