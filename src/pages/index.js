import { graphql, Link, useStaticQuery } from "gatsby";
import React from "react";
import Helmet from "react-helmet";
import Header from "../components/header";

const Index = () => {
  const data = useStaticQuery(graphql`
    query MyQuery {
      site {
        siteMetadata {
          title
        }
      }
    }
  `);

  return (
    <div>
      <Helmet>
        <title>{data.site.siteMetadata.title}</title>
      </Helmet>

      <Header />

      <h1>Powersites</h1>
      <Link to="/contato">Contato</Link>
    </div>
  );
};

export default Index;
