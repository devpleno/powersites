const path = require("path");

module.exports.createPages = ({ graphql, actions }) => {
  const { createPage } = actions;
  const contadorTemplate = path.resolve("src/templates/contador.js");

  [1, 2, 3, 4].forEach(p => {
    createPage({
      path: "/contador-" + p,
      component: contadorTemplate,
      context: {
        page: p
      }
    });
  });
};
